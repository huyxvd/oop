﻿class Rectangle : Shape
{
    // đây chính là tính đóng gói: che giấu thông tin
    private readonly int _width;
    private readonly int _heigh;
    public Rectangle(int width, int heigh)
    {
        _width = width;
        _heigh = heigh;
    }

    // đây chính là đa hình mỗi đối tượng của class khác nhau thì hành vi sẽ khác nhau
    public override double GetArea() => _width * _heigh;

    // đây chính là đa hình mỗi đối tượng của class khác nhau thì hành vi sẽ khác nhau
    public override double GetPerimeter() => (_width + _heigh) * 2;
}